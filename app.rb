require File.expand_path('../rackester', __FILE__)

class Aplicacion < Rackester::Base
  get '/prueba' do |env|
    [200, {"Content-Type" => "text/plain"}, ["parametros "+ env.params.inspect]]
  end

  post '/prueba' do |env|
    hola = "Hola gonzo " + Time.now.to_s
    [200, {"Content-Type" => "text/plain"}, [hola]]
  end
end
