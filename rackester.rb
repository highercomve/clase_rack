# require 'rack'

module Rackester

  class Base
    @@routes = []

    def call(env) 
      @env = env
      @request = Rack::Request.new(env)
      @method = @request.request_method.downcase
      @route = @request.path_info
      process_call
		end

		def routes
			@@routes
		end

    def process_call
      proceso = @@routes.select {|r| r[0] == @method && r[1] == @route  }.first
      if proceso
        proceso[2].call(@request)
      else
        [404, {"Content-Type" => "text/plain"}, ["Pagina no encontrada"]]
      end
    end

    def self.save_route method, route, block
      @@routes << [method, route, block]
    end

    def self.put route, &block
      save_route 'put', route, block
    end
    def self.get route, &block
      save_route 'get', route, block
    end
    def self.post route, &block
      save_route 'post', route, block
    end
  end
end


